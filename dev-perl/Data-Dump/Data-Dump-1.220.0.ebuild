# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header$

EAPI=4

MODULE_AUTHOR=GAAS
MODULE_VERSION=1.22
inherit perl-module

DESCRIPTION="Pretty printing of data structures"

SLOT="0"
KEYWORDS="~alpha ~amd64 ~arm ~hppa ~mips ~ppc ~x86 ~amd64-linux ~x86-linux ~x86-solaris"
IUSE=""

SRC_TEST=do
