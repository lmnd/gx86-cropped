# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header$

EAPI=4

MODULE_AUTHOR=INGY
MODULE_VERSION=0.15
inherit perl-module

DESCRIPTION="Spiffy Perl Interface Framework For You"

SLOT="0"
KEYWORDS="~amd64 hppa ia64 ~ppc ppc64 sparc x86"
IUSE=""

SRC_TEST="do"
