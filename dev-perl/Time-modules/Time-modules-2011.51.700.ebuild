# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header$

EAPI=4

MODULE_AUTHOR=MUIR
MODULE_SECTION=modules
MODULE_VERSION=2011.0517
inherit perl-module

DESCRIPTION="A Date/Time Parsing Perl Module"

LICENSE="Time-modules public-domain"
SLOT="0"
KEYWORDS="amd64 ppc ppc64 x86"
IUSE=""

SRC_TEST="do"
