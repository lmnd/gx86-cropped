# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header$

EAPI=4

MODULE_AUTHOR=RCAPUTO
MODULE_VERSION=1.351
inherit perl-module

DESCRIPTION="Reusable tests for POE::Loop authors"

SLOT="0"
KEYWORDS="amd64 ppc x86"
IUSE=""

SRC_TEST="do"
