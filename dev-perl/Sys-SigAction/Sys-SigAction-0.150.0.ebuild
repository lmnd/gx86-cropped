# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header$

EAPI=4

MODULE_AUTHOR=LBAXTER
MODULE_VERSION=0.15
inherit perl-module

DESCRIPTION="Perl extension for Consistent Signal Handling"

SLOT="0"
KEYWORDS="amd64 ~arm ~ppc x86"
IUSE=""

SRC_TEST=do
