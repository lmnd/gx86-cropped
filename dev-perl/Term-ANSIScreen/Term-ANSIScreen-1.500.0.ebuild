# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header$

EAPI=4

MODULE_AUTHOR=AUDREYT
MODULE_VERSION=1.50
inherit perl-module

DESCRIPTION="Terminal control using ANSI escape sequences"

LICENSE="CC0-1.0"
SLOT="0"
KEYWORDS="amd64 hppa ~ppc x86"
IUSE=""

SRC_TEST="do"
