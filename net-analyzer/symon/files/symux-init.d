#!/sbin/runscript
# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the 2-clause BSD license
# $Header$

opts="${opts} reload"

depend() {
	after bootmisc
	need localmount net
	use logger
}

reload() {
	ebegin "Reloading symux"
	start-stop-daemon --stop --pidfile /var/run/symux.pid \
		--exec /usr/sbin/symux --oknodo --signal HUP
	eend $?
}

start() {
	ebegin "Starting symux"
	start-stop-daemon --start --exec /usr/sbin/symux
	eend $?
}

stop() {
	ebegin "Stopping symux"
	start-stop-daemon --stop --pidfile /var/run/symux.pid
	eend $?
}
