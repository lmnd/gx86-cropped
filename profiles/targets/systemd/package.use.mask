# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header$

# 'static-libs' support on sys-apps/systemd is not provided
virtual/udev static-libs
sys-fs/cryptsetup static static-libs
sys-fs/lvm2 static static-libs
sys-fs/dmraid static
