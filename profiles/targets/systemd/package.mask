# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header$

# sys-kernel/genkernel is not compatible with Systemd, you need
# to use sys-kernel/genkernel-next instead
sys-kernel/genkernel
