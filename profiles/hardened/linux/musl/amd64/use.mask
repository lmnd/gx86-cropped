# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header$

# Unmask the flag which corresponds to ARCH.
-amd64

# Unmask the multilib flags for this arch.
-abi_x86_64

# unmask all SIMD assembler flags
-mmx
-mmxext
-sse
-sse2
-sse3
-sse4
-sse4a
-ssse3
-3dnow
-3dnowext

# Lilo works on amd64
-lilo

# These work
-input_devices_synaptics
-input_devices_wacom
-video_cards_qxl
