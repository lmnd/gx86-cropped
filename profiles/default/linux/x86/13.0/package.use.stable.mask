# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header$

# Tom Wijsman <TomWij@gentoo.org (16 Mar 2014)
# Mask unstable USE flags on media-video/vlc, see security bug #499806.
media-video/vlc gnutls opus vdpau

# Tom Wijsman <TomWij@gentoo.org> (06 Feb 2014)
# [QA] Masked jit USE flag on www-apps/cgit as dev-lang/luajit is not stable.
www-apps/cgit jit

# Michał Górny <mgorny@gentoo.org> (16 Nov 2013)
# Don't apply stable masks to python-exec since we're forcing every
# impl there anyway. Please keep this in sync with use.stable.mask.
dev-lang/python-exec -python_targets_pypy2_0
dev-lang/python-exec -python_single_target_pypy2_0
dev-python/python-exec -python_targets_pypy2_0
dev-python/python-exec -python_single_target_pypy2_0
