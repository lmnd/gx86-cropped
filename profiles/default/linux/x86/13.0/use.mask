# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header$

# Michał Górny <mgorny@gentoo.org> (19 Jan 2013)
# PyPy is keyworded on this arch.
-python_targets_pypy2_0
-python_single_target_pypy2_0
