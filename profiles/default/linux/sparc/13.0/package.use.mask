# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header$

# Anthony G. Basile <blueness@gentoo.org> (Dec 21, 2011)
#
# Mask the vde flag on tinc because net-misc/vde is not
# keyworded for sparc
net-misc/tinc vde
