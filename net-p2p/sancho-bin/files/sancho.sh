#!/bin/bash

# Copyright 1999-2004 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: ./gentoo-x86-cvsroot/net-p2p/sancho-bin/files/sancho.sh,v 1.4 2006/07/04 14:28:43 squinky86 Exp $

cd /opt/sancho
./sancho-bin ${*}
