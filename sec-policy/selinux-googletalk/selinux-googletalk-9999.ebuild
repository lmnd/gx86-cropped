# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header$
EAPI="4"

IUSE=""
MODS="googletalk"
BASEPOL="9999"

inherit selinux-policy-2

DESCRIPTION="SELinux policy for googletalk"

KEYWORDS=""
