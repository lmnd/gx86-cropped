# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header$

EAPI="2"
USE_RUBY="ruby18"

inherit ruby-ng-gnome2

DESCRIPTION="Ruby GConf2 bindings"
KEYWORDS="amd64 ppc x86"
IUSE=""
DEPEND="${DEPEND}
	>=gnome-base/gconf-2
	virtual/pkgconfig"
RDEPEND="${RDEPEND}
	>=gnome-base/gconf-2"

ruby_add_rdepend ">=dev-ruby/ruby-glib2-${PV}"
