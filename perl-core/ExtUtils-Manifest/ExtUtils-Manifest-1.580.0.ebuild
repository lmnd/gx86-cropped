# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header$

EAPI=2

MODULE_AUTHOR=RKOBES
MODULE_VERSION=1.58
inherit perl-module

DESCRIPTION="Utilities to write and check a MANIFEST file"

SLOT="0"
KEYWORDS="alpha amd64 ~arm arm64 hppa ia64 ppc ~ppc64 s390 sh sparc x86"
IUSE=""

SRC_TEST="do"
PREFER_BUILDPL="no"
