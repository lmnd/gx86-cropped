# Copyright 1999-2004 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: ./gentoo-x86-cvsroot/net-irc/srvx/files/srvx.conf.d,v 1.1.1.1 2005/11/30 09:48:57 chriswhite Exp $

# user and group to run srvx as
SRVX_USER="srvx"
SRVX_GROUP="srvx"
