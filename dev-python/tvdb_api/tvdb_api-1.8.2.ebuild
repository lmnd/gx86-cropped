# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header$

EAPI=4

inherit distutils vcs-snapshot

SRC_URI="https://github.com/dbr/${PN}/tarball/${PV} -> ${P}.tar.gz"
DESCRIPTION="Python interface to thetvdb.com API"
HOMEPAGE="http://github.com/dbr/tvdb_api"
SLOT="0"
KEYWORDS="~amd64 ~x86"
LICENSE="public-domain"
IUSE=""
DEPEND="dev-python/setuptools"
RDEPEND=""
