# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header$

EAPI=5
PYTHON_COMPAT=( python{2_6,2_7,3_2,3_3} pypy2_0 )

inherit distutils-r1 vcs-snapshot

DESCRIPTION="Python MPD client library"
HOMEPAGE="https://github.com/Mic92/python-mpd2"
SRC_URI="https://github.com/Mic92/${PN}2/tarball/v${PV} -> ${P}.tar.gz"

LICENSE="LGPL-3"
KEYWORDS="~amd64 ~ppc ~ppc64 ~x86"
SLOT="0"
IUSE="test"

DEPEND="test? ( virtual/python-unittest2[${PYTHON_USEDEP}]
	dev-python/mock[${PYTHON_USEDEP}] )"

DOCS=( CHANGES.txt README.md doc/commands.txt )

python_test() {
	"${PYTHON}" test.py || die "Tests fail with ${EPYTHON}"
}
