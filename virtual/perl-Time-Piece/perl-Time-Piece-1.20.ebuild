# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header$

DESCRIPTION="Object Oriented time objects"
HOMEPAGE=""
SRC_URI=""

LICENSE=""
SLOT="0"
KEYWORDS="alpha amd64 arm64 ia64 ppc ppc64 sparc x86 ~x86-solaris"
IUSE=""

RDEPEND="~perl-core/Time-Piece-${PV}"
