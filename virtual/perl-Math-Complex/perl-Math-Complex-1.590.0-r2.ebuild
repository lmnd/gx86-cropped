# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header$

DESCRIPTION="Virtual for perl-core/Math-Complex"
HOMEPAGE=""
SRC_URI=""

LICENSE=""
SLOT="0"
KEYWORDS="~amd64 ~hppa ~x86"
IUSE=""

DEPEND=""
RDEPEND="|| ( =dev-lang/perl-5.18* =dev-lang/perl-5.16* ~perl-core/Math-Complex-${PV} )"
