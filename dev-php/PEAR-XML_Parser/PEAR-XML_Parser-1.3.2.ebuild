# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: ./gentoo-x86-cvsroot/dev-php/PEAR-XML_Parser/PEAR-XML_Parser-1.3.2.ebuild,v 1.3 2010/03/27 23:12:29 mabi Exp $

inherit php-pear-r1

DESCRIPTION="XML parsing class based on PHP's SAX parser"

LICENSE="PHP-3"
SLOT="0"
KEYWORDS="~alpha ~amd64 ~arm ~hppa ~ia64 ~ppc ~ppc64 ~s390 ~sh ~sparc ~x86"
IUSE=""
